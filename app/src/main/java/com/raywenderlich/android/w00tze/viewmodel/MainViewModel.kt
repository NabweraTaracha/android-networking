package com.raywenderlich.android.w00tze.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.net.Uri
import com.raywenderlich.android.w00tze.BuildConfig
import com.raywenderlich.android.w00tze.app.Injection
import com.raywenderlich.android.w00tze.model.AccessToken
import com.raywenderlich.android.w00tze.model.AuthenticationPrefs
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(application: Application) : AndroidViewModel(application) {

    // Property to get AuthApi
    private val authApi = Injection.provideAuthApi()

    // Method to tell if user is authenticated
    fun isAuthenticated() = AuthenticationPrefs.isAuthenticated()

    // Method getAccessToken, to call into the authentication API.
    fun getAccessToken(uri: Uri, callback: () -> Unit) {

        // Pull an access code off the URI parameter passed in
        val accessCode = uri.getQueryParameter("code")


        // Call the callback lambda parameter on a success response,
        // along with saving the authentication token and token type into authentication prefs.
        authApi.getAccessToken(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, accessCode)
                .enqueue(object : Callback<AccessToken> {
                    override fun onResponse(call: Call<AccessToken>?, response: Response<AccessToken>?) {
                        val accessToken = response?.body()?.accessToken
                        val tokenType = response?.body()?.tokenType

                        if (accessToken != null && tokenType != null) {
                            AuthenticationPrefs.saveAuthToken(accessToken)
                            AuthenticationPrefs.saveTokenType(tokenType)
                            callback()
                        }
                    }

                    override fun onFailure(call: Call<AccessToken>, t: Throwable) {
                        println("ERROR GETTING TOKEN")
                    }
                })
    }

    //  Logout method; to log the user out of Github in our app.
    fun logout() {
        AuthenticationPrefs.saveAuthToken("")
        AuthenticationPrefs.clearUsername()
    }

}