package com.raywenderlich.android.w00tze.model

/**
 * An Either instance, is either a success or a failure & there are factory objects in the companion
 * object to create both.
 *
 * Either has a nullable generic data property of type T and a nullable error ApiError property.
 */
data class Either<out T>(val status: Status, val data: T?, val error: ApiError?) {

    companion object {
        fun <T> success(data: T?): Either<T> {
            return Either(Status.SUCCESS, data, null)
        }

        fun <T> error(error: ApiError, data: T?): Either<T> {
            return Either(Status.ERROR, data, error)
        }
    }
}