package com.raywenderlich.android.w00tze.repository

import com.raywenderlich.android.w00tze.model.Gist
import com.raywenderlich.android.w00tze.model.Repo
import com.raywenderlich.android.w00tze.model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubApi {

    @GET("users/{user}/repos")
    fun getRepos(@Path("user") user: String): Call<List<Repo>> // Function to get user repos

    @GET("users/{user}/gists")
    fun getGists(@Path("user") user: String): Call<List<Gist>> // Function to get user gists

    @GET("users/{user}")
    fun getUser(@Path("user") user: String): Call<User>
}